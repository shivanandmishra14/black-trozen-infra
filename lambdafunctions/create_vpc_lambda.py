import boto3
from botocore.exceptions import ClientError
import logging

# Initialize logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def lambda_handler(event, context):
    ec2_resource = boto3.resource('ec2')
    ec2_client = boto3.client('ec2')

    try:
        # Create VPC
        vpc = ec2_resource.create_vpc(CidrBlock='10.0.0.0/16')
        vpc.create_tags(Tags=[{'Key': 'Name', 'Value': 'TrozenRansomVPC'}])
        vpc.wait_until_available()
        logger.info(f"Created VPC with ID: {vpc.id}")

        # Create Internet Gateway and attach to VPC
        ig = ec2_resource.create_internet_gateway()
        vpc.attach_internet_gateway(InternetGatewayId=ig.id)
        ig.create_tags(Tags=[{'Key': 'Name', 'Value': 'DarkWebGateway'}])
        logger.info(f"Created and attached Internet Gateway with ID: {ig.id} to VPC with ID: {vpc.id}")

        # Create Subnet
        subnet = vpc.create_subnet(CidrBlock='10.0.1.0/24')
        subnet.create_tags(Tags=[{'Key': 'Name', 'Value': 'BlackHorseVirusSubnet'}])
        logger.info(f"Created Subnet with ID: {subnet.id}")

        # Create Route Table
        route_table = vpc.create_route_table()
        route_table.create_tags(Tags=[{'Key': 'Name', 'Value': 'BlackHorseVirusRouteTable'}])
        route_table.associate_with_subnet(SubnetId=subnet.id)
        route_table.create_route(DestinationCidrBlock='0.0.0.0/0', GatewayId=ig.id)
        logger.info(f"Created Route Table with ID: {route_table.id} and associated it with Subnet with ID: {subnet.id}")

        # Create Security Group
        sg = ec2_resource.create_security_group(GroupName='DarkNetSecurityGroup', Description='Allow SSH inbound traffic', VpcId=vpc.id)
        sg.authorize_ingress(
            CidrIp='0.0.0.0/0',
            IpProtocol='tcp',
            FromPort=22,
            ToPort=22
        )
        logger.info(f"Created Security Group with ID: {sg.id}")

        # Launch EC2 Instance
        instance = ec2_resource.create_instances(
            ImageId='ami-053b0d53c279acc90',  # Replace with valid AMI ID for your region
            InstanceType='t2.micro',
            MaxCount=1,
            MinCount=1,
            NetworkInterfaces=[{
                'SubnetId': subnet.id,
                'DeviceIndex': 0,
                'AssociatePublicIpAddress': True,
                'Groups': [sg.group_id]
            }],
            KeyName='blacktrozen'  # Ensure this key pair exists
        )[0]
        logger.info(f"Launched EC2 Instance with ID: {instance.id}")

        return {
            'statusCode': 200,
            'body': {
                'VPC_ID': vpc.id,
                'InternetGateway_ID': ig.id,
                'Subnet_ID': subnet.id,
                'RouteTable_ID': route_table.id,
                'SecurityGroup_ID': sg.id,
                'EC2Instance_ID': instance.id
            }
        }

    except ClientError as e:
        logger.error(f"An error occurred: {e}")
        return {
            'statusCode': 400,
            'body': {
                'error': str(e)
            }
        }

