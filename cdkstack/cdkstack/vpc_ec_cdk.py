from aws_cdk import core
from aws_cdk.aws_ec2 import Vpc, CfnInternetGateway, CfnVPCGatewayAttachment, CfnRouteTable, CfnRoute, CfnSubnetRouteTableAssociation, Instance, InstanceType, MachineImage
from aws_cdk.aws_iam import Role, ServicePrincipal, ManagedPolicy
import aws_cdk.aws_ec2 as ec2
import aws_cdk.aws_iam as iam

class PythonCdkProjectStack(core.Stack):
    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # Define the VPC
        vpc = Vpc(self, "TrojanRansomVPC",
                  max_azs=2,
                  cidr="10.0.0.0/16")

        # Create an internet gateway and attach it to the VPC
        internet_gateway = CfnInternetGateway(self, "DarkWebGateway")
        CfnVPCGatewayAttachment(self, "DarkWebGatewayAttachment",
                                vpc_id=vpc.vpc_id,
                                internet_gateway_id=internet_gateway.ref)

        # Create a route table and add a default route to the internet gateway
        route_table = CfnRouteTable(self, "BlackHorseVirusRouteTable",
                                     vpc_id=vpc.vpc_id)
        CfnRoute(self, "BlackHorseVirusRoute",
                 route_table_id=route_table.ref,
                 destination_cidr_block="0.0.0.0/0",
                 gateway_id=internet_gateway.ref)

        # Associate the route table with the subnets
        for subnet in vpc.public_subnets:
            CfnSubnetRouteTableAssociation(self, f"{subnet.node.id}RouteTableAssociation",
                                           subnet_id=subnet.subnet_id,
                                           route_table_id=route_table.ref)

        # Create an IAM Role for the EC2 instance
        role = Role(self, "RogueAgentRole",
                    assumed_by=ServicePrincipal("ec2.amazonaws.com"),
                    managed_policies=[ManagedPolicy.from_aws_managed_policy_name("AmazonSSMManagedInstanceCore")])

        # Create an EC2 instance within the VPC
        instance = Instance(self, "BlackHorseVirusMachine",
                            instance_type=InstanceType("t3.micro"),
                            machine_image=MachineImage.latest_amazon_linux(),
                            vpc=vpc,
                            role=role)

        # Output the instance ID
        core.CfnOutput(self, "BlackHorseVirusMachineID", value=instance.instance_id)


app = core.App()
PythonCdkProjectStack(app, "PythonCdkProjectStack")
app.synth()

